﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BorderBehaviour : MonoBehaviour
{
    public Image borderImage;
    public BorderManagerBehaviour borderManager;
	
	void Start ()
    {
        borderImage = GetComponent<Image>();
	}
	
	void Update ()
    {
        borderImage.color = borderManager.myColor;
	}
}
