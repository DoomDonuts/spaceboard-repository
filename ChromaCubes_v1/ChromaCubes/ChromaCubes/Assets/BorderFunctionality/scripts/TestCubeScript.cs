﻿using UnityEngine;
using System.Collections;

public class TestCubeScript : MonoBehaviour
{
    public Color myColor;
    public Color[] cubeColors;

    public bool colorMatches;
    public bool colorDoesntMatch;

    public ScoreScript scoreScript;
    public GameObject scoreScriptObject;

    public BorderManagerBehaviour borderManager;
    public GameObject borderManagerObject;
   
	void Start ()
    {
       // scoreScriptObject = GameObject.FindGameObjectWithTag("ScoreText");
        //scoreScript = scoreScriptObject.GetComponent<ScoreScript>();

        borderManagerObject = GameObject.FindGameObjectWithTag("BorderManager");
        borderManager = borderManagerObject.GetComponent<BorderManagerBehaviour>();

        myColor = borderManager.borderColors[Random.Range(0, 6)];
    }
	
	void Update ()
    {

        Renderer rend;
        rend = GetComponent<Renderer>();
        rend.material.color = myColor;

        if (myColor == borderManager.myColor)
        {
             colorMatches = true;
             colorDoesntMatch = false;
        }

        if (myColor != borderManager.myColor)
        {
             colorDoesntMatch = true;
             colorMatches = false;
        }

        if (Input.GetKeyDown(KeyCode.Return) && colorMatches)
        {
            scoreScript.myScore++;
            print("success!");
            //score increase
            //destroy cube
        }

        if (Input.GetKeyDown(KeyCode.Return) && colorDoesntMatch)
        {
            print("You dun goofed");
        }

        if(Input.GetKeyDown(KeyCode.Q))
        {
            myColor = borderManager.borderColors[Random.Range(0, 6)];
        }
    }
}
