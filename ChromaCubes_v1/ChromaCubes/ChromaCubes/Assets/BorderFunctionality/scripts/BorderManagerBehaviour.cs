﻿using UnityEngine;
using System.Collections;

public class BorderManagerBehaviour : MonoBehaviour
{
    public Color[] borderColors;
    public Color myColor;
	
	void Awake ()
    {
        borderColors = new Color[6];
        borderColors[0] = Color.blue;
        borderColors[1] = Color.cyan;
        borderColors[2] = Color.green;
        borderColors[3] = Color.magenta;
        borderColors[4] = Color.red;
        borderColors[5] = Color.yellow;

        myColor = borderColors[Random.Range(0, 6)];
    } 
	
	void Update ()
    {
         if(Input.GetKeyDown(KeyCode.Space))
        {
            myColor = borderColors[Random.Range(0, 6)];
        }
    } 
}
