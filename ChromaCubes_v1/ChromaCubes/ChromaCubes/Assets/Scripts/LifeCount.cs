﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LifeCount : MonoBehaviour
{
    public Text lifeCountText;
    public int lifeCount;

    void Update()
    {
        lifeCountText.text = "Lives: " + lifeCount;
        checkIfGameOver();
    }

    void checkIfGameOver()
    {
        if(lifeCount == 0)
        {
            SceneManager.LoadScene(0);
        }
    }
}
