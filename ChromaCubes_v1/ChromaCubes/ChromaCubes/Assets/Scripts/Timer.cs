﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public Text timerText;
    public int timer;

    void Start()
    {
        InvokeRepeating("timerCountdown", 1f, 1f);
    }

    void Update()
    {
        timerText.text = "Timer: " + timer;
    }

    private void timerCountdown()
    {
        timer -= 1;
    }
}
