﻿using UnityEngine;
using System.Collections;

public class CubeLeft : MonoBehaviour
{
    private Rigidbody cubeRB;
    public float cubeSpeed;
    public float rotateSpeed;
    public float timeToDestroy;

    void Start()
    {
       cubeRB = GetComponent<Rigidbody>();
        StartCoroutine("TimeToDestroy", 0f);
    }

    void FixedUpdate()
    {
        cubeRB.AddForce(Vector3.right * cubeSpeed);
        transform.Rotate(0, rotateSpeed, 0);
    }

    IEnumerator TimeToDestroy()
    {
        yield return new WaitForSeconds(timeToDestroy);
        Destroy(gameObject);
    }
}
