﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct CubeSpawnerPos
{
	public GameObject[] cubeSpawnerTop;
	public GameObject[] cubeSpawnerBottom;
	public GameObject[] cubeSpawnerLeft;
	public GameObject[] cubeSpawnerRight;
}
public class CubeSpawner : MonoBehaviour 
{
	public CubeSpawnerPos cubeSpawnerPos;
	public GameObject[] cube;
	public int getNumber;
    public bool canSpawn = false;
    public bool canGetNumber = true;
    public GameObject lifeCube;
    public GameObject bonusPointCube;

	void Start()
	{
		getReferences();
	}

	void Update()
	{
        if (canGetNumber == true)
        {
            canGetNumber = false;
            getNumber = Random.Range(1, 4);
            canSpawn = true;
        }

        spawnChecker();
	}

    #region SpawnChecker
    private void spawnChecker()
    {
        if (getNumber == 1 && canSpawn == true)
        {
            int specialNumber = Random.Range(0, 50);

            if (specialNumber == 1)
            {
                int getPosition = Random.Range(0, cubeSpawnerPos.cubeSpawnerTop.Length);
                Instantiate(lifeCube, cubeSpawnerPos.cubeSpawnerTop[getPosition].transform.position, Quaternion.identity);
                StartCoroutine("GetNumber", 0f);
                canSpawn = false;
            }

            else if (specialNumber == 2)
            {
                int getPosition = Random.Range(0, cubeSpawnerPos.cubeSpawnerTop.Length);
                Instantiate(bonusPointCube, cubeSpawnerPos.cubeSpawnerTop[getPosition].transform.position, Quaternion.identity);
                StartCoroutine("GetNumber", 0f);
                canSpawn = false;
            }

            else
            {
                int getPosition = Random.Range(0, cubeSpawnerPos.cubeSpawnerTop.Length);
                Instantiate(cube[0], cubeSpawnerPos.cubeSpawnerTop[getPosition].transform.position, Quaternion.identity);
                StartCoroutine("GetNumber", 0f);
                canSpawn = false;
            }
        }

        if (getNumber == 2 && canSpawn == true)
        {
            int specialNumber = Random.Range(0, 50);

            if (specialNumber == 1)
            {
                int getPosition = Random.Range(0, cubeSpawnerPos.cubeSpawnerTop.Length);
                Instantiate(lifeCube, cubeSpawnerPos.cubeSpawnerTop[getPosition].transform.position, Quaternion.identity);
                StartCoroutine("GetNumber", 0f);
                canSpawn = false;
            }

            else if (specialNumber == 2)
            {
                int getPosition = Random.Range(0, cubeSpawnerPos.cubeSpawnerTop.Length);
                Instantiate(bonusPointCube, cubeSpawnerPos.cubeSpawnerTop[getPosition].transform.position, Quaternion.identity);
                StartCoroutine("GetNumber", 0f);
                canSpawn = false;
            }

            else
            {
                int getPosition = Random.Range(0, cubeSpawnerPos.cubeSpawnerBottom.Length);
                Instantiate(cube[1], cubeSpawnerPos.cubeSpawnerBottom[getPosition].transform.position, Quaternion.identity);
                StartCoroutine("GetNumber", 0f);
                canSpawn = false;
            }
        }

        if (getNumber == 3 && canSpawn == true)
        {
            int specialNumber = Random.Range(0, 50);

            if (specialNumber == 1)
            {
                int getPosition = Random.Range(0, cubeSpawnerPos.cubeSpawnerTop.Length);
                Instantiate(lifeCube, cubeSpawnerPos.cubeSpawnerTop[getPosition].transform.position, Quaternion.identity);
                StartCoroutine("GetNumber", 0f);
                canSpawn = false;
            }

            else if (specialNumber == 2)
            {
                int getPosition = Random.Range(0, cubeSpawnerPos.cubeSpawnerTop.Length);
                Instantiate(bonusPointCube, cubeSpawnerPos.cubeSpawnerTop[getPosition].transform.position, Quaternion.identity);
                StartCoroutine("GetNumber", 0f);
                canSpawn = false;
            }

            else
            {
                int getPosition = Random.Range(0, cubeSpawnerPos.cubeSpawnerLeft.Length);
                Instantiate(cube[2], cubeSpawnerPos.cubeSpawnerLeft[getPosition].transform.position, Quaternion.identity);
                StartCoroutine("GetNumber", 0f);
                canSpawn = false;
            }
        }

        if (getNumber == 4 && canSpawn == true)
        {
            int specialNumber = Random.Range(0, 50);

            if (specialNumber == 1)
            {
                int getPosition = Random.Range(0, cubeSpawnerPos.cubeSpawnerTop.Length);
                Instantiate(lifeCube, cubeSpawnerPos.cubeSpawnerTop[getPosition].transform.position, Quaternion.identity);
                StartCoroutine("GetNumber", 0f);
                canSpawn = false;
            }

            else if (specialNumber == 2)
            {
                int getPosition = Random.Range(0, cubeSpawnerPos.cubeSpawnerTop.Length);
                Instantiate(bonusPointCube, cubeSpawnerPos.cubeSpawnerTop[getPosition].transform.position, Quaternion.identity);
                StartCoroutine("GetNumber", 0f);
                canSpawn = false;
            }


            else
            {
                int getPosition = Random.Range(0, cubeSpawnerPos.cubeSpawnerRight.Length);
                Instantiate(cube[3], cubeSpawnerPos.cubeSpawnerRight[getPosition].transform.position, Quaternion.identity);
                StartCoroutine("GetNumber", 0f);
                canSpawn = false;
            }
        }
    }
    #endregion

    private void getReferences()
	{
		cubeSpawnerPos.cubeSpawnerTop = GameObject.FindGameObjectsWithTag("cubeSpawnerTop");
		cubeSpawnerPos.cubeSpawnerBottom = GameObject.FindGameObjectsWithTag("cubeSpawnerBottom");
		cubeSpawnerPos.cubeSpawnerLeft = GameObject.FindGameObjectsWithTag("cubeSpawnerLeft");
		cubeSpawnerPos.cubeSpawnerRight = GameObject.FindGameObjectsWithTag ("cubeSpawnerRight");
	}

	IEnumerator GetNumber()
	{
		yield return new WaitForSeconds(0.5f);
        canGetNumber = true;
	}
}
