﻿using UnityEngine;
using System.Collections;

public class CubeBottom1 : MonoBehaviour
{
    private Rigidbody cubeRB;
    public float cubeSpeed;
    public float rotateSpeed;
    public float timeToDestroy;

    void Start()
    {
        cubeRB = GetComponent<Rigidbody>();
        StartCoroutine("TimeToDestroy", 0f);
    }

    void FixedUpdate()
    {
        cubeRB.AddForce(Vector3.up * cubeSpeed);
        transform.Rotate(rotateSpeed, 0, 0);
    }

    IEnumerator TimeToDestroy()
    {
        yield return new WaitForSeconds(timeToDestroy);
        Destroy(gameObject);
    }
}
